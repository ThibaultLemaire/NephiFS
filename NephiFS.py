#!/usr/bin/env python


import logging

import os
import errno
import AddressPacker
from BlockDevice import BlockDevice
from FileTree import Tree
from DBusInterface import DBusInterface
from sys import argv, exit
from stat import S_IFDIR, S_IFREG, S_IFLNK

from fuse import FUSE, FuseOSError, Operations, LoggingMixIn
	
		

class NephiFS(LoggingMixIn, Operations):
	def __init__(self, blockPath, mntpt=u'/'):
		self.attr = dict((key, getattr(os.lstat(mntpt), key)) for key in ('st_atime', 'st_ctime',
            'st_gid', 'st_mode', 'st_mtime', 'st_nlink', 'st_uid'))
		self.namemax = 255
		self.bd = BlockDevice(blockPath)
		self.fsname="NephiFS"
		offset=len(self.fsname)
		formatted = self.bd.readData(0, offset) == self.fsname
		self.bd.writeData(0, self.fsname)
		self.bd.lateInit(offset, formatted)
		self.tree = Tree(self.bd, self.attr['st_mode'], offset, formatted)
		self.fd = 0
	
	def symlink(self, target, source):
		self.create(target, (S_IFLNK | 0o777))
		self.write(target, source, 0, 0)
	
	def readlink(self, path):
		return self.read(path, self.getattr(path)['st_size'], 0, 0)
	
	def mkdir(self, path, mode):
		self.tree[path] = dict(mode=(S_IFDIR | mode), st_nlink=2,
                                size=0,
                                addr=self.bd.allocate(),
                                content={})
 
	def create(self, path, mode):
		self.tree[path] = dict(mode=(S_IFREG | mode), st_nlink=1,
                                size=0,
                                addr=self.bd.allocate())
		return self.fd

	def getattr(self, path, fh=None):
		try:
			node = self.tree[path]
			attributes = self.attr
			attributes['st_mode'] = node['mode']
			attributes['st_size'] = node['size']
			attributes['st_nlink'] = node['st_nlink']
			return attributes
		except KeyError:
			raise FuseOSError(errno.ENODATA)
	
	def getxattr(self, path, name, position=0):
		if name == 'st_atime' or name == 'st_ctime' or name == 'st_mtime' or name == 'st_gid' or name == 'st_uid':
			return self.attr[name]
		elif name == 'st_mode' or name == 'st_size' or name == 'st_nlink':
			return self.tree[path][name]
		else:
			raise FuseOSError(errno.ENODATA)

	def open(self, path, flags):
		self.fd += 1
		return self.fd

	def readdir(self, path, fh):
		directory = self.tree[path]
		if 'content' not in directory:
			self.tree.loadEntries(directory)
		return ['.', '..'] + directory['content'].keys()

	def rename(self, old, new):
		self.tree[new] = self.tree.pop(old)


	def statfs(self, path):
		return dict(f_bsize=1, f_blocks=self.bd.size, f_bavail=self.getTotalFreeSpace(), f_namemax=self.namemax)


	def truncate(self, path, length, fh=None):
		truncatedSize = self.tree[path]['size'] - length
		if truncatedSize < 0:
			self.write(path, chr(0) * -truncatedSize, self.tree[path]['size'], 0)
		elif truncatedSize > 0:
			self.bd.truncate(truncatedSize, self.tree[path]['addr'])
		self.tree[path]['size'] = length

	def unlink(self, path):
		try:
			self.bd.freeFileAt(self.tree.pop(path)['addr'])
		except KeyError:
			raise FuseOSError(errno.ENOENT)
	
	def rmdir(self, path):
		if self.tree[path]['content'] == {}:
			self.unlink(path)
		else:
			raise FuseOSError(errno.ENOTEMPTY)
    
	def read(self, path, size, offset, fh):
		return self.bd.fragmentReader(self.tree[path]['addr'], size, offset)

	def write(self, path, data, offset, fh):
		if self.bd.fragmentWriter(data, self.tree[path]['addr'], offset):
			self.tree[path]['size'] = len(data)+offset
		return len(data)
    
	def destroy(self, path):
		self.bd.close()
		pass
	
	def chmod(self, path, mode):
		node = self.tree.pop(path)
		node['mode'] &= 0o770000
		node['mode'] |= mode
		self.tree[path] = node
		return 0
	
	def chown(self, path, uid, gid):
		self.attr['st_uid'] = uid
		self.attr['st_gid'] = gid
        
	
	access = None
	flush = None
	fsync = None
	link = None
	listxattr = None
	mknod = None
	release = None
	utimens = None
	
	# ==== Defragmenting ====
	
	def getFragmentationRate(self):
		ratiosum = 0
		filecount = 0
		for entry in self.tree.getEntries():
			filecount += 1
			ratiosum += self.bd.getFileFragRate(entry[1])
		return ratiosum / filecount
	
	def getFragmentsMap(self):
		fragmap = self.tree.getEntries()
		for entry in fragmap:
			entry[1] = self.bd.getFragments(entry[1])
		return fragmap
	
	def getNearestFrag(self, address, fragmap):
		pointer = fragmap[0][0]
		nearest = fragmap[0][1][0][0]
		tmppointer = pointer
		for entry in fragmap:
			tmppointer = entry[0]
			for frag in entry[1][0]:
				if frag[0] == address:
					pointer = tmppointer
					nearest = frag
				tmppointer = frag[0] + frag[1] + self.bd.pointersize
		return pointer, nearest
	
	def moveFragment(self, freespace, parent_addr, fragment):
		availablespace = freespace[1] - freespace[0] + 1 - self.bd.pointersize*2
		if type(parent_addr) is unicode:
			newentry = self.tree.pop(parent_addr)
			newentry['addr'] = freespace[0]
		if availablespace < fragment[1]:
			size = availablespace
			nextFragment = fragment[0] + size
			size_to_free = size
		else:
			size = fragment[1]
			nextFragment = self.bd.readAddress(fragment[0] + size + self.bd.pointersize)
			address_to_free = freespace[0] + size + self.bd.pointersize*2
			self.bd.free(address_to_free, freespace[1] - address_to_free + 1)
			size_to_free = fragment[1] + self.bd.pointersize*2
			
		data = AddressPacker.pack(size, self.bd.pointersize) + self.bd.readData(fragment[0] + self.bd.pointersize, size) + AddressPacker.pack(nextFragment, self.bd.pointersize)
		
		if availablespace < fragment[1]:
			self.bd.writeAddress(nextFragment, fragment[1] - size)
		
		self.bd.writeData(freespace[0], data)
		if type(parent_addr) is unicode:
			self.tree[parent_addr] = newentry
		else:
			self.bd.writeAddress(parent_addr, freespace[0])
		self.bd.free(fragment[0], size_to_free)
	
	def Defragment(self):
		entrymap = self.getFragmentsMap()
		fragmap = filter(lambda x : len(x[1][0]) > 1, entrymap)
		if len(fragmap) > 0:
			target = fragmap[0]
			for entry in fragmap:
				if entry[1][1] > target[1][1]:
					target = entry
			end_of_available_space = self.bd.size - self.bd.tablesize - self.bd.pointersize
			if target[1][1] > end_of_available_space - target[1][0][0][0]: # starting from its current start address will the remaining space till the end be too little for storing the whole file?
				freespace = self.bd.pop(self.bd.getFirstFreeSpaceBefore(end_of_available_space - target[1][1]))
				self.moveFragment(freespace, target[0], target[1][0][0])
			else:
				end_addr = target[1][0][0][0] + target[1][0][0][1] + self.bd.pointersize*2
				nextFSpace = self.bd.getNextFreeSpace(end_addr)
				if not nextFSpace:
					nextFSpace = self.bd.pop(self.bd.getMaxFreeSpace()[1])
					parent_addr, blocking_frag = self.getNearestFrag(end_addr, fragmap)
					self.moveFragment(nextFSpace, parent_addr, blocking_frag)
				else:
					availablespace = nextFSpace[1] - nextFSpace[0] + 1
					if availablespace < target[1][0][1][1]:
						size = availablespace
						nextFragment = target[1][0][1][0] + size
						size_to_free = size
					else:
						size = target[1][0][1][1]
						nextFragment = self.bd.readAddress(target[1][0][1][0] + size + self.bd.pointersize)
						address_to_free = nextFSpace[0] + size
						self.bd.free(address_to_free, nextFSpace[1] - address_to_free + 1)
						size_to_free = target[1][0][1][1] + self.bd.pointersize*2
						
					data = self.bd.readData(target[1][0][1][0] + self.bd.pointersize, size) + AddressPacker.pack(nextFragment, self.bd.pointersize)
					
					if availablespace < target[1][0][1][1]:
						self.bd.writeAddress(nextFragment, target[1][0][1][1] - size)
					
					self.bd.writeData(target[1][0][0][0] + target[1][0][0][1] + self.bd.pointersize, data)
					self.bd.writeAddress(target[1][0][0][0], target[1][0][0][1] + size)
					self.bd.free(target[1][0][1][0], size_to_free)
				
		return self.getFragmentationRate()
		
	
	#====Wrappers====
	
	def getTotalFreeSpace(self):
		return self.bd.getTotalFreeSpace()
	
	def getSize(self):
		return self.bd.size

	def getPointerSize(self):
		return self.bd.pointersize

	def getFreeSpacesRepresentation(self):
		return repr(self.bd.freeSpaces)

	def getEntriesRepresentation(self):
		return repr(self.tree.getEntries())
	

if __name__ == '__main__':
	if len(argv) != 3:
		print('usage: %s <block device> <mountpoint>' % argv[0])
		exit(1)

	logging.basicConfig(level=logging.DEBUG)
	
	fs = NephiFS(argv[1], argv[2])
	
	idbus = DBusInterface(fs, argv[1])
	
	idbus.daemon = True
	idbus.start()
	
	fuse = FUSE(fs, argv[2], foreground=True)
