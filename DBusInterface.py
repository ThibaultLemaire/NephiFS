#!/usr/bin/env python

import gobject
import dbus
import dbus.service
import threading
from dbus.mainloop.glib import DBusGMainLoop
from fuse import FuseOSError
import errno

DBusGMainLoop(set_as_default=True)

gobject.threads_init()



OPATH = "/NephiFS"
IFACE = "com.filesystem.NephiFS"
BUS_NAME = "com.filesystem.NephiFS."

def translatePath(path):
	return filter(None, path.split('/'))

class DBusInterface(dbus.service.Object, threading.Thread):
	def __init__(self, filesystem, blockpath):
		blockpath = translatePath(blockpath).pop()
		bus = dbus.SessionBus()
		if bus.request_name(BUS_NAME+blockpath) != 1:
			raise FuseOSError(errno.EBUSY)
		bus_name = dbus.service.BusName(BUS_NAME+blockpath, bus=bus)
		dbus.service.Object.__init__(self, bus_name, OPATH)
		threading.Thread.__init__(self)
		self.fs = filesystem
	
	def run(self):
		gobject.MainLoop().run()
	
	@dbus.service.method(dbus_interface=IFACE,
						in_signature="", out_signature="t")
	def getSize(self):
		return self.fs.bd.size
	
	@dbus.service.method(dbus_interface=IFACE,
						in_signature="", out_signature="i")
	def getPointerSize(self):
		return self.fs.bd.pointersize
		
	@dbus.service.method(dbus_interface=IFACE,
						in_signature="", out_signature="s")
	def getFreeSpacesRepresentation(self):
		return repr(self.fs.bd.freeSpaces)
		
	@dbus.service.method(dbus_interface=IFACE,
						in_signature="", out_signature="s")
	def getFileTreeRepresentation(self):
		return repr(self.fs.tree.Tree)
		
	@dbus.service.method(dbus_interface=IFACE,
						in_signature="", out_signature="s")
	def getEntriesRepresentation(self):
		return repr(self.fs.tree.getEntries())
		
	@dbus.service.method(dbus_interface=IFACE,
						in_signature="", out_signature="d")
	def getFragmentationRate(self):
		return self.fs.getFragmentationRate()
		
	@dbus.service.method(dbus_interface=IFACE,
						in_signature="", out_signature="d")
	def Defragment(self):
		return self.fs.Defragment()
		
	@dbus.service.method(dbus_interface=IFACE,
						in_signature="", out_signature="t")
	def getTotalFreeSpace(self):
		return self.fs.bd.getTotalFreeSpace()
