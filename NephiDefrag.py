#!/usr/bin/env python

import dbus
from NephiFS import NephiFS
from sys import argv, exit

OPATH = "/NephiFS"
BUS_NAME = "com.filesystem.NephiFS."

COMMANDS = ["info",
			"debug",
			"defrag"]

def translatePath(path):
	return filter(None, path.split('/'))

if __name__ == '__main__':
	if len(argv) != 3:
		print('usage: %s <blockdevice> <command>' % argv[0])
		exit(1)
	
	if argv[2] not in COMMANDS:
		print('unknown command: %s' % argv[1])
		exit(1)
	
	print("\n======= NephiFS Defragment Tool =======\n")
	
	bus = dbus.SessionBus()
	busname = BUS_NAME+translatePath(argv[1]).pop()
	if bus.request_name(busname) != 1:
		fs = bus.get_object(busname, OPATH)
	else:
		fs = NephiFS(argv[1])

	if argv[2] == COMMANDS[0]:
		print("\n===== Filesystem Information =====\n")
		freespace = fs.getTotalFreeSpace()
		size = fs.getSize()
		print('Total Size : ' + str(size))
		print
		print('Total Free Space : ' + str(freespace))
		print
		print('Total Used Space : ' + str(size - freespace))
		print
		print('Fragmentation : ' + str(fs.getFragmentationRate() * 100) + '%')
		print
		
	if argv[2] == COMMANDS[1]:
		print("\n===== Debug Info =====\n")
		print('PointerSize : ' + str(fs.getPointerSize()))
		print
		print('FreeSpaces table : ' + fs.getFreeSpacesRepresentation())
		print
		print('Entries table : ' + fs.getEntriesRepresentation())
		print
	
	if argv[2] == COMMANDS[2]:
		print("\n===== Defragmenting =====\n")
		fragmentation = fs.Defragment()
		while fragmentation > 0:
			fragmentation = fs.Defragment()
			print('Fragmentation is now : ' + str(fs.Defragment() * 100) + '%')
		print("Filesystem defragmented");

