#!/usr/bin/env python

def unpack(data):
	i = 0
	number = 0
	for character in data:
		number += ord(character)*256**i
		i += 1
	return number

def pack(integer, ptrsize):
	output = ""
	for i in range(ptrsize):
		output += chr(integer % 256)
		integer //= 256
	return output
