#!/usr/bin/env python

import AddressPacker
import errno

from threading import Lock, Thread
from fuse import FuseOSError

def getSize(item):
	return item[1] - item[0] +1

class BlockDevice(file):
	def __init__(self, path):
		super(BlockDevice, self).__init__(path, 'r+b')
		self.rwlock = Lock()
		self.seek(0, 2)
		self.size = self.tell()
		self.__freeSpaces = {}
		self.pointersize = 0
		i = self.size
		while i > 0:
			i //= 256
			self.pointersize += 1
		self.tablesize = self.pointersize*2*255
	
	@property
	def freeSpaces(self):
		return self.__freeSpaces
	
	def lateInit(self, offset, isFormatted):
		if isFormatted:
			data = self.readData(self.size-self.tablesize, self.tablesize)
			for i in range(self.tablesize // (self.pointersize*2), 0, -1):
				entry = [AddressPacker.unpack(data[:self.pointersize])]
				data = data[self.pointersize:]
				if entry[0] > 0:
					entry.append(AddressPacker.unpack(data[:self.pointersize]))
					self.__freeSpaces[i] = entry
				data = data[self.pointersize:]
		else:
			self.push([offset+self.pointersize*2, self.size-self.tablesize-1])
	
	def writeData(self, address, data):
		with self.rwlock:
			self.seek(address)
			self.write(data)
		
	def readData(self, address, size):
		with self.rwlock:
			self.seek(address)
			return self.read(size)
	
	def readAddress(self, address):
		return AddressPacker.unpack(self.readData(address, self.pointersize))
    
	def writeAddress(self, address, addressToWrite):
		self.writeData(address, AddressPacker.pack(addressToWrite, self.pointersize))
	
	def getFragments(self, nextFragment):
		fragments = []
		size = 0
		while nextFragment > 0:
			fragmentSize = self.readAddress(nextFragment)
			size += fragmentSize
			fragments.append([nextFragment, fragmentSize])
			nextFragment = self.readAddress(nextFragment + self.pointersize+fragmentSize)
		return fragments, size
    
	def fragmentReader(self, address, size, offset):
		data = ""
		fragmentSize = self.readAddress(address)
		beginningOfFile = address + self.pointersize
		endOfFragment = beginningOfFile+fragmentSize
		nextFragment = self.readAddress(endOfFragment)
		if offset > fragmentSize:
			if nextFragment == 0:
				raise FuseOSError(errno.EFAULT)
			else:
				data = self.fragmentReader(nextFragment, size, offset - fragmentSize)
		elif offset + size > fragmentSize:
			readableSize = fragmentSize - offset
			data = self.readData(beginningOfFile + offset, readableSize)
			if nextFragment > 0:
				data += self.fragmentReader(nextFragment, size-readableSize, 0)
		else:
			data = self.readData(beginningOfFile + offset, size)
		return data
    
    
	def fragmenter(self, data, slicepoint, address=0):
		if address == 0:
			address = self.allocate(len(data[:slicepoint]))
		extended = self.fragmentWriter(data[slicepoint:], address, 0)
		data = data[:slicepoint]
		return address, data, extended
    
	def fragmentWriter(self, data, address, offset):
		extended = False
		fragmentSize = self.readAddress(address)
		beginningOfFile = address + self.pointersize
		endOfFragment = beginningOfFile+fragmentSize
		nextFragment = self.readAddress(endOfFragment)
		if offset > fragmentSize:
			if nextFragment == 0:
				print("Address out of fragment")
				raise FuseOSError(errno.EFAULT)
			else:
				extended = self.fragmentWriter(data, nextFragment, offset - fragmentSize)
		else:
			relEndOfData = offset + len(data)
			if relEndOfData > fragmentSize:
				if nextFragment == 0:
					extended = True
					endOfData = beginningOfFile + relEndOfData
					newEndOfFile = endOfData + self.pointersize
					nextFreeSpaceBegin = endOfFragment+self.pointersize
					noFreeSpaceFound = True
					for index, freeSpace in self.items():
						if freeSpace[0] == nextFreeSpaceBegin:
							if newEndOfFile < freeSpace[1]:
								self.set(index, 0, newEndOfFile)
							else:
								self.pop(index)
								if newEndOfFile > freeSpace[1]:
									nextFragment, data = self.fragmenter(data, freeSpace[1] + 1 - beginningOfFile - offset - self.pointersize)[0:2]
									relEndOfData = offset + len(data)
									endOfData = beginningOfFile + relEndOfData
							self.writeAddress(endOfData, nextFragment)
							self.writeAddress(address, relEndOfData)
							noFreeSpaceFound = False
							break
					if noFreeSpaceFound:
						nextFragment, data = self.fragmenter(data, fragmentSize - offset)[0:2]
						self.writeAddress(endOfFragment, nextFragment)
				else:
					nextFragment, data, extended = self.fragmenter(data, fragmentSize - offset, nextFragment)
			self.writeData(beginningOfFile + offset, data)
		return extended
	
	def truncate(self, truncatedSize, address):
		fragments = self.getFragments(address)[0] # every fragment is a list of two elements : [address, size]
		while truncatedSize > 0:
				fragment = fragments.pop()
				if fragment[1] < truncatedSize:
					self.free(fragment[0], fragment[1]+self.pointersize*2)
					truncatedSize -= fragment[1]
				elif fragment[1] > truncatedSize:
					newSize = fragment[1]-truncatedSize
					newEnd = fragment[0] + newSize + self.pointersize
					self.writeAddress(newEnd, 0) # writing eof
					self.writeAddress(fragment[0], newSize) # updating fragment size
					self.free(newEnd+self.pointersize, truncatedSize)
					truncatedSize = 0
				else:
					if len(fragments) > 0: # is the current fragment the last of the file?
						self.free(fragment[0], fragment[1]+self.pointersize*2) # freeing the surrounding pointers as well
						fragment = fragments.pop() # we jump to the fragment before the current one
					else:
						# Ending up here means this truncate operation intends to shrink the file size down to 0.
						# However a file cannot take no disk space for it always needs to have
						# a Size Pointer at the beginning and an EOF Pointer at the end.
						# Therefore we mustn't free them along with the last fragment
						# Or they would be freed again with a later attempt to unlink the file.
						self.free(fragment[0]+self.pointersize*2, fragment[1])
						
						self.writeAddress(fragment[0], 0) # updating fragment size
						fragment[1] = self.pointersize # ensuring the next line will work in both cases
					self.writeAddress(fragment[0] + fragment[1], 0) # writing eof flag
					truncatedSize = 0 # to ensure this is the last iteration of the loop
		
		
	def freeFileAt(self, nextFragment):
		while nextFragment != 0:
			fragmentSize = self.readAddress(nextFragment)
			self.free(nextFragment, fragmentSize+self.pointersize*2)
			nextFragment = self.readAddress(nextFragment + self.pointersize+fragmentSize)
	
	def items(self):
		return self.__freeSpaces.iteritems()
	
	def pop(self, index):
		with self.rwlock:
			self.seek( -index*self.pointersize*2, 2)
			self.write(AddressPacker.pack(0, self.pointersize*2))
		return self.__freeSpaces.pop(index)
	
	delete = pop
	
	def set(self, key, index, value):
		if type(value) is int:
			self.__freeSpaces[key][index] = value
			value = AddressPacker.pack(value, self.pointersize)
		elif type(value) is list:
			index = 0
			self.__freeSpaces[key] = value
			value = AddressPacker.pack(value[0], self.pointersize) + AddressPacker.pack(value[1], self.pointersize)
		with self.rwlock:
			self.seek( -key*self.pointersize*2 + index*self.pointersize, 2)
			self.write(value)
	
	def push(self, item):
		freeIndex = 1
		while freeIndex in self.__freeSpaces:
			freeIndex += 1
		if freeIndex > self.tablesize:
			raise FuseOSError(errno.ENFILE)
		self.set(freeIndex, 0, item)
	
	append = push
	
	def getMaxFreeSpace(self):
		targetFreeSpace = 0
		targetIndex = 0
		totalAvailable = 0
		for index, space in self.items():
			freeSpace = getSize(space)
			totalAvailable += freeSpace
			if freeSpace > targetFreeSpace:
				targetFreeSpace = freeSpace
				targetIndex = index
		return targetFreeSpace, targetIndex, totalAvailable
	
	def allocate(self, size=0):
		targetFreeSpace, targetIndex, totalAvailable = self.getMaxFreeSpace()
		if totalAvailable > size:
			targetAddress = self.__freeSpaces[targetIndex][0] + targetFreeSpace // 3
			self.push([targetAddress + self.pointersize*2, self.__freeSpaces[targetIndex][1]])
			self.set(targetIndex, 1, targetAddress-1)
			self.writeData(targetAddress, AddressPacker.pack(0, self.pointersize*2))
			return targetAddress
		else:
			raise FuseOSError(errno.ENOSPC)
			
	def getNextFreeSpace(self, endOfFragment):
		for index, freeSpace in self.items():
			if freeSpace[0] == endOfFragment:
				return self.pop(index)
		return None
	
	def free(self, address, size):
		if size > 0:
			endOfFragment = address + size
			prevFreeSpace = -1
			nextFreeSpace = -1
			for index, freeSpace in self.items():
				if freeSpace[1] == address-1:
					prevFreeSpace = index
				if freeSpace[0] == endOfFragment:
					nextFreeSpace = index
				if prevFreeSpace > -1 and nextFreeSpace > -1:
					break
			if prevFreeSpace > -1 and nextFreeSpace > -1:
				self.set(prevFreeSpace, 1, self.__freeSpaces[nextFreeSpace][1])
				self.pop(nextFreeSpace)
			elif prevFreeSpace > -1:
				self.set(prevFreeSpace, 1, endOfFragment-1)
			elif nextFreeSpace > -1:
				self.set(nextFreeSpace, 0, address)
			else:
				self.push([address, endOfFragment-1])
	
	def getTotalFreeSpace(self):
		total = 0
		for index, space in self.items():
			total += getSize(space)
		return total
	
	def getFileFragRate(self, address):
		return 1 - float(1) / len(self.getFragments(address)[0])
		
	def isFree(self, address):
		for index, freespace in self.__freeSpaces.items():
			if freespace[0] <= address:
				if freespace[1] >= address:
					return True
		return False
	
	def getFirstFreeSpaceBefore(self, address):
		spacefound = [float("-inf"),float("inf")]
		targetindex = 0
		for index, freespace in self.__freeSpaces.items():
			if freespace[0] <= address and freespace[0] > spacefound[0]:
				targetindex = index
				spacefound = freespace
		return index
