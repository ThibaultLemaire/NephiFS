# [NephiFS]

is a school project from 2016. It is a filesystem written in python for the [FUSE] kernel module.

As of now, it is largely undocumented and isn't intended to be used on real hardware, but it is [BSD licenced](LICENCE) and could be used as a reference implementation for [fusepy].

It implements most of the features you would expect from a filesystem (files, directories/subdirectories, UTF-8 names, etc.), tries to avoid fragmentation -- albeit in a pretty dumb way (1/3rd biggest free space allocation) -- but is painfully slow (~14kB/s r/w).

Oh and it does come with a multi-context (offline & online) defragmentation tool. But it doesn't work. -ish. Let's say that given the right conditions, it does end up defragmenting the partition. With some time.

Do not hesitate to open an issue if you encounter any problem with or if you have any question about it.

## Dependencies

### Arch

``` zsh
pacaur -Syu python2-fusepy
```

## Usage

Here is an example to get you started:

``` zsh
truncate _100K.bd --size=100K
mkdir mountpoint
python2 NephiFS.py _100K.bd mountpoint
```

There you go, you now have a 100kB volume mounted on `mounpoint` that is formatted in NephiFS. Go ahead and try to add files, folders, and subdirectories. Try to find out the limits of the filesystem.

- `_100K.bd` is a fake [block device], conventionally suffixed `.bd` (so it is ignored by `git`). If [NephiFS] was a real filesystem, this block device would be `/dev/sda2` for example.
- `mountpoint` is the directory where we mount the filesystem. It could be any directory, but note that **it should be empty**.
- You can display the usage syntax with `python2 NephiFS.py --help`

[NephiFS]: https://gitlab.com/ThibaultLemaire/NephiFS
[block device]: https://en.wikipedia.org/wiki/Device_file#Block_devices
[FUSE]: https://en.wikipedia.org/wiki/Filesystem_in_Userspace
[fusepy]: https://pypi.python.org/pypi/fusepy
