#!/usr/bin/env python
# -*- coding: utf-8 -*-


import errno
import AddressPacker
from fuse import FuseOSError
from time import time

def translatePath(path):
	return filter(None, path.split('/'))

class Tree():
	def __init__(self, blockDevice, mode, address, isFormatted):
		self.bd = blockDevice
		self.entrySize = 257 + self.bd.pointersize
		self.__Tree = dict(mode=mode,
			st_nlink=2,
			content={},
			addr=address,
			index=0)
		if isFormatted:
			self.__Tree['size'] = self.bd.getFragments(address)[1]
			self.loadEntries(self.__Tree)
		else:
			self.__Tree['size'] = 0
			self.bd.writeData(address, AddressPacker.pack(0, self.bd.pointersize*2))
			
	
	@property
	def Tree(self):
		return self.__Tree
	
	
	def __getitem__(self, path):
		if type(path) is unicode:
			path = translatePath(path)
		node = self.__Tree
		for directory in path:
			if node['mode'] / 1000 == 16: # if the node is a directory
				if 'content' not in node:
					self.loadEntries(node)
				if directory in node['content']: # if there is an entry by that name
					node = node['content'][directory]
				else:
					raise FuseOSError(errno.ENOENT)
			else:
				raise FuseOSError(errno.ENOTDIR)
		return node
		
	
	def __setitem__(self, path, node):
		if node == self.__Tree:
			raise FuseOSError(errno.EBADF)
		if type(path) is unicode:
			path = translatePath(path)
		name = path.pop()
		parent = self[path]
		indexes = []
		for key, value in parent['content'].items():
			indexes.append(value['index'])
		index = 0
		while index in indexes:
			index += 1
		node['index'] = index
		parent['content'][name] = node
		#   Writing the new entry ↓                                              at ↓   with offset ↓
		if self.bd.fragmentWriter(self.packEntry(node['mode'], name, node['addr']), parent['addr'], index*self.entrySize):
			parent['size'] += self.entrySize
	
	def pop(self, path):
		if type(path) is unicode:
			path = translatePath(path)
		name = path.pop()
		parent = self[path]
		node = parent['content'].pop(name)
		localAddress = node['index']*self.entrySize
		reducedSize = parent['size'] - self.entrySize
		if reducedSize == localAddress:
			parent['size'] = reducedSize
			self.bd.truncate(self.entrySize, parent['addr'])
		else:
			self.bd.fragmentWriter(AddressPacker.pack(0, self.bd.pointersize), parent['addr'], localAddress + 257)
		return node
	
	def packEntry(self, mode, entryName, address):
		name = entryName.encode("utf-8")
		name += chr(0) * (255 - len(name))
		return AddressPacker.pack(mode, 2) + name + AddressPacker.pack(address, self.bd.pointersize)
	
	def loadEntries(self, node):
		data = self.bd.fragmentReader(node['addr'], node['size'], 0)
		size = len(data)
		if size % self.entrySize == 0:
			node['content'] = {}
			for i in range(0, size // self.entrySize):
				entry = data[:self.entrySize]
				data = data[self.entrySize:]
				address = AddressPacker.unpack(entry[257:])
				if address > 0:
					name = filter(lambda x: x != chr(0), entry[2:257].decode("utf-8"))
					mode = AddressPacker.unpack(entry[0:2])
					if mode / 1000 == 16:
						st_nlink = 2
					else:
						st_nlink = 1
					node['content'][name] = dict(mode=mode,
												st_nlink=st_nlink,
												size=self.bd.getFragments(address)[1], # getFragments returns a tuple of which the second element is the size of the file
												addr=address,
												index=i)
		else:
			print (size, self.entrySize, size % self.entrySize)
			raise FuseOSError(errno.EFAULT)

	# ==== Helpers ====
	
	def getEntries(self):
		entries = [[u'/', self.__Tree['addr']]]
		firstnode = self.__Tree.copy()
		firstnode['path'] = ""
		layer = [firstnode]
		while layer != []:
			newlayer = []
			for directory in layer:
				if 'content' not in directory:
					self.loadEntries(directory)
				for entryname, entry in directory['content'].items():
					newentryname = directory['path']+u'/'+entryname
					entries.append([newentryname, entry['addr']])
					if entry['mode'] / 1000 == 16: # if the entry is a directory
						newentry = entry.copy()
						newentry['path'] = newentryname
						newlayer.append(newentry)
			layer = newlayer
		return entries
